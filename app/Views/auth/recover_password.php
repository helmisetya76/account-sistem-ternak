<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Account Sistem Ternak | Recover Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('themes/plugins/fontawesome-free/css/all.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('themes/plugins/icheck-bootstrap/icheck-bootstrap.min'); ?>'">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('themes/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <!-- <a href="../../index2.html"><b>Admin</b>LTE</a> -->
            <img src='https://www.sistemternak.com/wp-content/uploads/sites/2/2021/04/512-Lite-Icon.png' width='100px' height='100px'>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Masukkan kembali password baru anda</p>

                <!-- <form action="<?php echo $_ENV['app.api.apps_lite'] . '/users/changePass' ?>" method="post"> -->
                <form>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id='txtPass' placeholder="Password" onkeyup="cekPass()">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id='idUser' name='idUser' value="<?php echo $user['id']; ?>" />
                    <input type="hidden" id='token' name='token' value="<?php echo $token; ?>" />
                    <input type="hidden" id="linkApi" value="<?php echo $_ENV['app.api.apps_lite'] . '/users/changePass' ?>" />
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id='txtPassConfirm' name='newPassword' placeholder="Konfirmasi Password" onkeyup="cekPass()">

                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <span id='message'></span>

                </form>
                <div class="row">
                    <div class="col-12">

                        <button type="submit" class="btn btn-primary btn-block" onclick="updatePass()">Ganti password</button>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?php echo base_url('themes/plugins/jquery/jquery.min.js') ?>">

    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url('themes/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('themes/dist/js/adminlte.min.js') ?>"></script>
    <script>
        function cekPass() {
            if (document.getElementById('txtPass').value == document.getElementById('txtPassConfirm').value) {
                if (document.getElementById('txtPass').value.length < 6) {
                    document.getElementById('message').style.color = 'red';
                    document.getElementById('message').innerHTML = 'Password minimal 6 karakter';
                } else {
                    document.getElementById('message').style.color = 'white';
                    document.getElementById('message').innerHTML = 'Password tidak sama';
                }
            } else {
                document.getElementById('message').style.color = 'red';
                document.getElementById('message').innerHTML = 'Password tidak sama';
            }
        }

        function updatePass() {
            var idUser = document.getElementById('idUser').value
            var token = document.getElementById('token').value
            var newPassword = document.getElementById('txtPassConfirm').value
            $.ajax({
                method: 'POST',
                url: document.getElementById('linkApi').value,
                cache: false,
                data: {
                    'newPassword': newPassword,
                    'idUser': idUser,
                    'token' : token
                },
                success: function(json) {
                    console.log(json)
                    if (json.status == 200) {
                        var base_url = window.location.origin
                        // alert('berhasil')
                        location.href = base_url + '/auth/succes_reset_password'
                    } else {
                        var base_url = window.location.origin
                        // alert('berhasil')
                        location.href = base_url + '/auth/fail-reset-password/?idUser=' + idUser+'&token='+token
                    }
                },
                error: function(throwError) {
                    console.log(throwError)
                    // location.reload()
                    var base_url = window.location.origin
                    // alert('berhasil')
                    location.href = base_url + '/auth/fail_reset_password/?idUser=' + idUser+'&token='+token
                }
            })
        }
    </script>

</body>

</html>