<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Account Sistem Ternak</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('themes/plugins/fontawesome-free/css/all.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url('themes/plugins/icheck-bootstrap/icheck-bootstrap.min'); ?>'">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('themes/dist/css/adminlte.min.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <!-- <div class="error-page"> -->
            <div class="error-content" style='padding-left:10px'>
                <div class="login-logo" style='padding-top:10px'>
                    <!-- <a href="../../index2.html"><b>Admin</b>LTE</a> -->
                    <img src='https://www.sistemternak.com/wp-content/uploads/sites/2/2021/04/512-Lite-Icon.png' width='100px' height='100px'>
                </div>
                <!-- <h2 class="headline text-success">Sukses</h2> -->
                <?php 
                    if($status == true){?>
                        <h3><i class="fas fa-check-square text-success"></i> Berhasil!!!</h3>
                        <p>
                            Password berhasil diganti, Silahkan login kembali.
                        </p>
                    <?php }else{ ?>
                        <h3><i class="fas fa-window-close text-danger"></i> Gagal!!!</h3>
                        <p>
                            Password gagal diganti, Silahkan <a href="<?php echo $_ENV['app.baseURL']."/auth/recoverpassword?idUser=".$idUser."&token=".$token; ?>">reset password</a> kembali.
                        </p>
                    <?php }
                ?>
                
            </div>
            <!-- </div> -->
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
    <!-- jQuery -->
    <script src="<?php echo base_url('themes/plugins/jquery/jquery.min.js') ?>">
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url('themes/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('themes/dist/js/adminlte.min.js') ?>"></script>
</body>
</html>