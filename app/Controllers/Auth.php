<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;

class Auth extends Controller{
    public function __construct() {
        $this->userData = new UserModel();
    }
    public function recoverPassword(){
        $idUser = $_GET['idUser'];
        $token = $_GET['token'];
        // $idUser = 
        $data['user'] = $this->userData->getProduct($idUser);
        $data['token']=$token;
        return view('auth/recover_password',$data);
    }
    public function succes_reset_password(){
        $data['status']=true;
        return view('auth/result_reset_password',$data);
    }
    public function fail_reset_password(){
        $data['status']=false;
        $data['idUser']=$_GET['idUser'];
        $token = $_GET['token'];
        $data['token']=$token;
        return view('auth/result_reset_password',$data);
    }
}